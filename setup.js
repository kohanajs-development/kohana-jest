const path = require("node:path");
const fs = require("node:fs");
const Database = require("better-sqlite3");
const {ORM, ControllerMixinDatabase, KohanaJS} = require("kohanajs");
const {ORMAdapterSQLite, DatabaseDriverBetterSQLite3} = require("@kohanajs/mod-database-adapter-better-sqlite3");

class KohanaJS_Jest{
  static init(opt){
    const helperPath = `${opt.DIR}/mockapp`

    const results = this.copyDB(
      `${helperPath}/defaultDB`,
      `${helperPath}/db`,
      opt.DATABASES )

    ORM.defaultAdapter = ORMAdapterSQLite;
    ControllerMixinDatabase.DEFAULT_DATABASE_DRIVER = DatabaseDriverBetterSQLite3;

    KohanaJS.init({
      EXE_PATH: `${helperPath}/application`,
      APP_PATH: `${helperPath}/application`,
    });
    opt.PRELOAD.forEach(it => KohanaJS.classPath.set(it[0], it[1]));
    KohanaJS.initConfig(new Map([
      ['cookie', {
        salt: 'theencryptsaltatleast32character',
        options: {
          secure: false,
          maxAge: 86400000,
        },
      }],
      ['session', {
        secret: { kty: 'oct', alg: 'HS256', key_ops: ['sign', 'verify'], k: '5Mskt4P-gXn74SJ1kmNiBSZXv6QQXR5uRpCzF9KuTuGg5vz-3aUlD-TZ0V7MGvkhTE-X0b6Av5gdOwTqSj6jgQ', ext: true },
        saveUninitialized: false,
        resave: false,
        name: 'kohanajs-session',
        cookie: {
          secure: false,
          httpOnly: true,
          sameSite: 'Strict',
          maxAge: 14400,
        },
      }],
      ...opt.CONFIGS.map(it => [it, ''])
    ]));

    return results;
  }

  static copyDB(sourceFolder, targetFolder, databaseFiles=[]){
    return databaseFiles.map(file =>{
      const target = path.normalize(targetFolder + '/' + file);
      if (fs.existsSync(target))fs.unlinkSync(target);
      fs.copyFileSync(path.normalize(sourceFolder + '/'+ file), target);
      return new Database(target);
    });
  }
}

module.exports = KohanaJS_Jest;